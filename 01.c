#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern void quicksort(int* a, int lo, int hi);

void findTwoThatSumTo2020(int* values, int length) {
  int i = 0;
  int j = length - 1;

  while (i < j) {
    int left = values[i];
    int right = values[j];
    if (left + right > 2020) {
      j -= 1;
    } else if (left + right < 2020) {
      i += 1;
    } else {
      printf("%d\n", left * right);
      break;
    }
  }  
}

void findThreeThatSumTo2020(int* values, int length) {
  for (int i = 0; i < length; i += 1) {
    int l = i + 1;
    int r = length - 1;

    while (l < r) {
      int sum = values[i] + values[l] + values[r];
      if (sum < 2020) {
	l += 1;
      } else if (sum > 2020) {
	r -= 1;
      } else {
	printf("%d\n", values[i] * values[l] * values[r]);
	break;
      }
    }
  }
}

int main() {
  const char* FILE_NAME = "01-input.txt";
  char c[5];
  memset(c, 0, 5);
  
  FILE* input;
  if ((input = fopen(FILE_NAME, "r")) == NULL) {
    printf("Error opening %s", FILE_NAME);
    exit(1);
  }

  int lineCount = 0;
  while (EOF != fscanf(input, "%[^\n]\n", c)) {
    lineCount += 1;
  }

  fseek(input, 0, SEEK_SET);
  int* values = calloc(lineCount, sizeof(int));
  lineCount = 0;
  while (EOF != fscanf(input, "%[^\n]", c)) {
    values[lineCount] = atoi(c);
    fscanf(input, "\n");
    lineCount += 1;
  }

  quicksort(values, 0, lineCount - 1);

  findTwoThatSumTo2020(values, lineCount);
  findThreeThatSumTo2020(values, lineCount);

  fclose(input);
  free(values);
  return 0;
}
