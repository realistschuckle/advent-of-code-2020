#include <stdio.h>
#include <stdlib.h>
#include <string.h>

long countTrees(char* entries, int lines, int columns, int dx, int dy) {
  int x = 0;
  long total = 0;
  for (int y = 0; y < lines; y += dy, x = (x + dx) % columns) {
    if (entries[y * columns + x] == '#') {
      total += 1;
    }
  }
  return total;
}

int main() {
  const char* FILE_NAME = "03-input.txt";

  FILE* input;
  if ((input = fopen(FILE_NAME, "r")) == NULL) {
    printf("Error opening %s", FILE_NAME);
    exit(1);
  }

  int columnCount = 0;
  int lineCount = 0;
  char c;
  int s = 0;
  while (EOF != (c = fgetc(input))) {
    lineCount += (c == '\n' ? 1 : 0);
    columnCount += (lineCount == 0 ? 1 : 0);
    s += 1;
  }

  fseek(input, 0, SEEK_SET);
  char *entries = calloc(lineCount * columnCount + 1, sizeof(char));
  memset(entries, 0, lineCount * columnCount + 1);
  int i = 0;
  while (EOF != (c = fgetc(input))) {
    if (c != '\n') {
      entries[i] = c;
      i += 1;
    }
  }

  long three_one = countTrees(entries, lineCount, columnCount, 3, 1);
  printf("%d\n", three_one);

  long one_one = countTrees(entries, lineCount, columnCount, 1, 1);
  long five_one = countTrees(entries, lineCount, columnCount, 5, 1);
  long seven_one = countTrees(entries, lineCount, columnCount, 7, 1);
  long one_two = countTrees(entries, lineCount, columnCount, 1, 2);
  printf("%ld\n", one_one * three_one * five_one * seven_one * one_two);

  fclose(input);
  free(entries);
  return 0;
}
