#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

struct passport {
  int birthYear;
  int issueYear;
  int expirationYear;
  char height[15];
  char hairColor[15];
  char eyeColor[15];
  char passportId[15];
  int countryId;
};

static unsigned getFileSize(const char* fileName) {
  struct stat s;
  if (stat(fileName, &s) != 0) {
    return -1;
  }
  return s.st_size;
}

void setProperty(struct passport* p, const char* name, const char* value) {
  if (strncmp(name, "byr", 3) == 0) {
    p->birthYear = atoi(value);
  } else if (strncmp(name, "iyr", 3) == 0) {
    p->issueYear = atoi(value);
  } else if (strncmp(name, "eyr", 3) == 0) {
    p->expirationYear = atoi(value);
  } else if (strncmp(name, "hgt", 3) == 0) {
    strcpy(p->height, value);
  } else if (strncmp(name, "hcl", 3) == 0) {
    strcpy(p->hairColor, value);
  } else if (strncmp(name, "ecl", 3) == 0) {
    strcpy(p->eyeColor, value);
  } else if (strncmp(name, "pid", 3) == 0) {
    strcpy(p->passportId, value);
  } else if (strncmp(name, "cid", 3) == 0) {
    p->countryId = atoi(value);
  } else {
    printf("UNKNOWN PROPERTY: %s\n", name);
  }
}

bool validatePassport1(struct passport *p) {
  return p->birthYear > 0
    && p->issueYear > 0
    && p->expirationYear > 0
    && strlen(p->height) > 0
    && strlen(p->hairColor) > 0
    && strlen(p->eyeColor) > 0
    && strlen(p->passportId) > 0;
}

bool validateHeight(const char* height) {
  int hl = strlen(height);
  char value[20] = { 0 };
  bool good = false;
  if (hl == 5 && height[hl - 2] == 'c' && height[hl - 1] == 'm') {
    strncpy(value, height, 3);
    int hv = atoi(value);
    good = 150 <= hv && hv <= 193;
  }
  if (hl == 4 && height[hl - 2] == 'i' && height[hl - 1] == 'n') {
    strncpy(value, height, 2);
    int hv = atoi(value);
    good = 59 <= hv && hv <= 76;
  }
  return good;
}

bool validateHairColor(const char* color) {
  const char* allowedLetters = "0123456789abcdef";
  int colorLength = strlen(color);
  
  if (colorLength != 7) return false;
  if (color[0] != '#') return false;

  bool all = true;
  for (int i = 1; i < colorLength && all; i += 1) {
    bool some = false;
    for (int j = 0; j < 16; j += 1) {
      if (color[i] == allowedLetters[j]) {
	some = true;
      }
    }
    all = all && some;
  }
  return all;
}

bool validateEyeColor(const char* color) {
  return strlen(color) == 3
    && (
	strncmp(color, "amb", 3) == 0
	|| strncmp(color, "blu", 3) == 0
	|| strncmp(color, "brn", 3) == 0
	|| strncmp(color, "gry", 3) == 0
	|| strncmp(color, "grn", 3) == 0
	|| strncmp(color, "hzl", 3) == 0
	|| strncmp(color, "oth", 3) == 0
	);
}

bool validatePassport2(struct passport *p) {
  return 1920 <= p-> birthYear && p->birthYear <= 2002
    && 2010 <= p->issueYear && p->issueYear <= 2020
    && 2020 <= p->expirationYear && p-> expirationYear <= 2030
    && validateHeight(p->height)
    && validateHairColor(p->hairColor)
    && validateEyeColor(p->eyeColor)
    && strlen(p->passportId) == 9 && atol(p->passportId) > 0;
}

int main() {
  const char* FILE_NAME = "04-input.txt";

  int size = getFileSize(FILE_NAME);
  if (size < 0) {
    printf("Error finding %s\n", FILE_NAME);
    return 2;
  }

  FILE* input;
  if ((input = fopen(FILE_NAME, "r")) == NULL) {
    printf("Error opening %s\n", FILE_NAME);
    return 1;
  }
  char* content = calloc(sizeof(char), size + 1);
  memset(content, 0, size + 1);

  int numberOfPassports = 0;
  int i;
  char c, pc;
  for (i = 0, c = fgetc(input), pc = 0; c != EOF; i += 1, pc = c, c = fgetc(input)) {
    content[i] = c;
    if (pc == c && c == '\n') {
      numberOfPassports += 1;
    }
  }
  numberOfPassports += 1;

  struct passport *passports = calloc(sizeof(struct passport), numberOfPassports);
  memset(passports, 0, sizeof(struct passport) * numberOfPassports);
  
  int j;
  int current;
  char propertyName[4] = { 0 };
  char propertyValue[20] = { 0 };
  for (i = 0, j = 1, current = 0; j < size; j += 1) {
    if (content[j] == ':') {
      strncpy(propertyName, content + i, 3);
      i = j + 1;
    } else if (content[j] == ' ' || content[j] == '\t') {
      strncpy(propertyValue, content + i, j - i);
      propertyValue[j - i] = '\0';
      setProperty(&(passports[current]), propertyName, propertyValue);
      i = j + 1;
    } else if (content[j] == '\n') {
      if (content[j - 1] == '\n') {
	current += 1;
	i = j + 1;
	continue;
      }
      strncpy(propertyValue, content + i, j - i);
      propertyValue[j - i] = '\0';
      setProperty(&(passports[current]), propertyName, propertyValue);
      i = j + 1;
    }
  }

  int validPassports1 = 0;
  int validPassports2 = 0;
  for (i = 0; i < numberOfPassports; i += 1) {
    if (validatePassport1(&(passports[i]))) {
      validPassports1 += 1;
    }
    if (validatePassport2(&(passports[i]))) {
      validPassports2 += 1;
    }
  }
  printf("%d\n", validPassports1);
  printf("%d\n", validPassports2);

  fclose(input);
  free(passports);
  free(content);
  return 0;
}
