#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern void quicksort(int* a, int lo, int hi);

int main() {
  const char* FILE_NAME = "05-input.txt";

  FILE* input;
  if ((input = fopen(FILE_NAME, "r")) == NULL) {
    printf("Error opening %s\n", FILE_NAME);
    return 1;
  }

  int numberOfSeats = 0;
  int maxNumberOfSeats = 1024;
  int* seats = calloc(maxNumberOfSeats, sizeof(int));
  
  int bestRow = 0;
  int bestSeat = 0;
  int bestId = 0;

  char c = 0;
  int index = 0;
  int endRow = 127;
  int startRow = 0;
  int endSeat = 7;
  int startSeat = 0;
  while (EOF != (c = fgetc(input))) {
    if (c == '\n') {
      if (endRow * 8 + endSeat > bestId) {
	bestRow = endRow;
	bestSeat = endSeat;
	bestId = endRow * 8 + endSeat;
      }
      seats[numberOfSeats] = endRow * 8 + endSeat;
      numberOfSeats += 1;

      if (numberOfSeats > maxNumberOfSeats) {
	maxNumberOfSeats *= 2;
	seats = realloc(seats, maxNumberOfSeats * sizeof(int));
      }

      index = 0;
      endRow = 127;
      startRow = 0;
      endSeat = 7;
      startSeat = 0;
      continue;
    } else if (index < 7) {
      if (c == 'F') {
	endRow = endRow - (endRow + 1 - startRow) / 2;
      } else {
	startRow = startRow + (endRow + 1 - startRow) / 2;
      }
    } else {
      if (c == 'L') {
	endSeat = endSeat - (endSeat + 1 - startSeat) / 2;
      } else {
	startSeat = startSeat + (endSeat + 1 - startSeat) / 2;
      }
    }
    index += 1;
  }

  quicksort(seats, 0, numberOfSeats - 1);

  printf("%d\n", bestId);

  for (int i = 0; i < numberOfSeats - 1; i += 1) {
    if (seats[i] + 1 != seats[i + 1]) {
      printf("%d\n", seats[i] + 1);
    }
  }

  free(seats);
  fclose(input);
  return 0;
}
