#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ALPHABET_SIZE 26

extern void quicksort(int* a, int lo, int hi);

int main() {
  const char* FILE_NAME = "06-input.txt";

  FILE* input;
  if ((input = fopen(FILE_NAME, "r")) == NULL) {
    printf("Error opening %s\n", FILE_NAME);
    return 1;
  }

  int letters[ALPHABET_SIZE] = { 0 };
  int lettersByPerson[ALPHABET_SIZE] = { 0 };
  char c;
  char b = 0;
  int individualSum = 0;
  int groupSum = 0;
  int numberOfPeople = 0;
  
  while (EOF != (c = fgetc(input))) {
    if (b == '\n' && c == '\n') {
      for (int i = 0; i < ALPHABET_SIZE; i += 1) {
	individualSum += letters[i];
      }
      for (int i = 0; i < ALPHABET_SIZE; i += 1) {
	if (lettersByPerson[i] == numberOfPeople) {
	  groupSum += 1;
	}
      }
      memset(letters, 0, sizeof(int) * ALPHABET_SIZE);
      memset(lettersByPerson, 0, sizeof(int) * ALPHABET_SIZE);
      numberOfPeople = 0;
    } else if (c == '\n') {
      numberOfPeople += 1;
    }
    b = c;
    letters[c - 'a'] = 1;
    lettersByPerson[c - 'a'] += 1;
  }
  for (int i = 0; i < ALPHABET_SIZE; i += 1) {
    individualSum += letters[i];
  }
  for (int i = 0; i < ALPHABET_SIZE; i += 1) {
    if (lettersByPerson[i] == numberOfPeople) {
      groupSum += 1;
    }
  }

  printf("%d\n", individualSum);
  printf("%d\n", groupSum);

  fclose(input);
  return 0;
}
