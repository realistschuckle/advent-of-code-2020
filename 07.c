#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern void quicksort(int* a, int lo, int hi);

#define MAX_BAG_LINKS 128
#define MAX_MODIFIERS 1024
#define MAX_MODIFIER_LENGTH 32
#define MAX_COLOR_LENGTH 32
#define MAX_MC_LENGTH MAX_MODIFIER_LENGTH + MAX_COLOR_LENGTH + 1

struct bag {
  int holdIndex;
  int holds[MAX_BAG_LINKS];
  int quantities[MAX_BAG_LINKS];
  
  int heldByIndex;
  int heldBy[MAX_BAG_LINKS];
};

enum parseState
  {
   MODIFIER,
   COLOR,
   BAGS,
   CONTAIN,
   QUANTITY,
  };

const char *STATES[5] = { "MODIFIER", "COLOR", "BAGS", "CONTAIN", "QUANTITY" };

int countBags(struct bag* bag, struct bag *bags) {
  int sum = 0;
  for (int i = 0; i < bag->holdIndex; i += 1) {
    int childId = bag->holds[i];
    int quantity = bag->quantities[i];
    struct bag* child = &bags[childId];
    int childCount = countBags(child, bags);
    sum += quantity * childCount;
  }
  return 1 + sum;
}

void associateBags(struct bag bags[MAX_MODIFIERS], int parentBagId, int childBagId, int quantity) {
  struct bag* parent = &bags[parentBagId];
  struct bag* child = &bags[childBagId];

  parent->holds[parent->holdIndex] = childBagId;
  parent->quantities[parent->holdIndex] = quantity;
  parent->holdIndex += 1;

  child->heldBy[child->heldByIndex] = parentBagId;
  child->heldByIndex += 1;
}

int modifierId(char modifiers[MAX_MODIFIERS][MAX_MC_LENGTH], char* modifier, char* color) {
  int i;
  for (i = 0; i < MAX_MODIFIERS && modifiers[i][0] != 0; i += 1) {
    bool found = true;
    int j;
    for (j = 0; j < strlen(modifier) && found; j += 1) {
      if (modifier[j] != modifiers[i][j]) {
	found = false;
      }
    }
    if (found) {
      if (modifiers[i][j] == ' ') {
	j += 1;
	int colorLen = strlen(color);
	for (int k = 0; k < colorLen && found; k += 1) {
	  if (color[k] != modifiers[i][j + k]) {
	    found = false;
	  }
	}
      }
    }
    if (found) {
      break;
    }
  }
  if (modifiers[i][0] == '\0') {
    sprintf(modifiers[i], "%s %s", modifier, color);
  }
  return i;
}

int main() {
  const char* FILE_NAME = "07-input.txt";

  FILE* input;
  if ((input = fopen(FILE_NAME, "r")) == NULL) {
    printf("Error opening %s\n", FILE_NAME);
    return 1;
  }

  fseek(input, 0, SEEK_END);
  int size = ftell(input);
  fseek(input, 0, SEEK_SET);
  char* content = calloc(size, sizeof(char) + 1);
  memset(content, 0, sizeof(char) + 1);
  fread(content, sizeof(char), size, input);
  fclose(input);

  char modifiers[MAX_MODIFIERS][MAX_MC_LENGTH] = { 0 };
  struct bag bags[MAX_MODIFIERS];
  for (int i = 0; i < MAX_MODIFIERS; i += 1) {
    bags[i].holdIndex = 0;
    bags[i].heldByIndex = 0;
    for (int j = 0; j < MAX_BAG_LINKS; j += 1) {
      bags[i].holds[j] = -1;
      bags[i].quantities[j] = -1;
      bags[i].heldBy[j] = -1;
    }
  }

  int activeBagId = 0;
  int quantity = -1;
  char* modifier = NULL;
  char* color = NULL;
  char* saveptr = NULL;
  char* token = strtok_r(content, " \n", &saveptr);
  enum parseState state = MODIFIER;
  while(token != NULL) {
    switch (state) {
    case MODIFIER: {
      modifier = token;
      state = COLOR;
      break;
    }
    case COLOR: {
      color = token;
      state = BAGS;
      break;
    }
    case BAGS: {
      int len = strlen(token);
      switch (token[len - 1]) {
      case 's': {
	state = CONTAIN;
	break;
      }
      case '.':
      case ',': {
	int newBagId = modifierId(modifiers, modifier, color);
	associateBags(bags, activeBagId, newBagId, quantity);
	state = token[len - 1] == '.' ? MODIFIER : QUANTITY;
	break;
      }
      }
      break;
    }
    case CONTAIN: {
      activeBagId = modifierId(modifiers, modifier, color);
      modifier = NULL;
      color = NULL;
      
      state = QUANTITY;
      break;
    }
    case QUANTITY: {
      if (strcmp("no", token) == 0) {
	strtok_r(NULL, " \n", &saveptr); // other
	strtok_r(NULL, " \n", &saveptr); // bags.
      } else {
	quantity = atoi(token);
      }
      state = MODIFIER;
      break;
    }
    }
    token = strtok_r(NULL, " \n", &saveptr);
  }

  int queue[MAX_BAG_LINKS * MAX_BAG_LINKS];
  memset(queue, -1, MAX_BAG_LINKS * MAX_BAG_LINKS);
  int j = 1;
  int shinyGoldId = modifierId(modifiers, "shiny", "gold");
  queue[0] = shinyGoldId;
  for (int i = 0; queue[i] != -1; i += 1) {
    for (int k = 0; k < bags[queue[i]].heldByIndex; k += 1, j += 1) {
      queue[j] = bags[queue[i]].heldBy[k];
    }
  }
  queue[0] = queue[j - 1];
  quicksort(queue, 0, j - 2);

  int sumHeldBy = 1;
  for (int i = 1; i < j - 1; i += 1) {
    if (queue[i] == queue[i - 1]) {
      continue;
    }
    sumHeldBy += 1;
  }
  printf("%d\n", sumHeldBy);

  struct bag* shinyGoldBag = &bags[shinyGoldId];
  int sumHolds = countBags(shinyGoldBag, bags) - 1;
  printf("%d\n", sumHolds);
  
  free(content);
  return 0;
}
