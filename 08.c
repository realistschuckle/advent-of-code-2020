#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_INSTRUCTIONS 1024

enum OpCode
  {
   ACC = 1,
   JMP = 2,
   NOP = 3
  };

struct instruction {
  enum OpCode opCode;
  int value;
  int runCount;
};

struct bootloaderOutput {
  int accumulator;
  bool ended;
};

enum ParseState
  {
   OP_CODE,
   VALUE
  };

struct bootloaderOutput runBootloader(struct instruction* instructions) {
  int programCounter = 0;
  int accumulator = 0;
  struct instruction* inst = &instructions[programCounter];
  while (inst->runCount == 0 && inst->opCode > 0) {
    inst->runCount += 1;
    switch (inst->opCode) {
    case ACC: {
      accumulator += inst->value;
      programCounter += 1;
      break;
    }
    case JMP: {
      programCounter += inst->value;
      break;
    }
    case NOP: {
      programCounter += 1;
      break;
    }
    }
    inst = &instructions[programCounter];
  }
  struct bootloaderOutput output = { accumulator, inst->opCode == 0 };
  return output;
}

int main() {
  const char* FILE_NAME = "08-input.txt";

  FILE* input;
  if ((input = fopen(FILE_NAME, "r")) == NULL) {
    printf("Error opening %s\n", FILE_NAME);
    return 1;
  }

  struct instruction instructions[MAX_INSTRUCTIONS];
  for (int i = 0; i < MAX_INSTRUCTIONS; i += 1) {
    instructions[i].opCode = 0;
    instructions[i].value = 0;
    instructions[i].runCount = 0;
  }
  
  int instructionIndex = 0;
  char value[6] = { '\0' };
  enum ParseState state = OP_CODE;
  char c;
  while ((c = fgetc(input)) != EOF) {
    switch (state) {
    case OP_CODE: {
      switch (c) {
      case 'n': {
	instructions[instructionIndex].opCode = NOP;
	break;
      }
      case 'a': {
	instructions[instructionIndex].opCode = ACC;
	break;
      }
      case 'j': {
	instructions[instructionIndex].opCode = JMP;
	break;
      }
      }
      fgetc(input);

      fgetc(input);
      fgetc(input);
      state = VALUE;
      break;
    }
    case VALUE: {
      memset(value, '\0', 6);
      for (int i = 0; c != '\n'; c = fgetc(input), i += 1) {
	value[i] = c;
      }
      instructions[instructionIndex].value = atoi(value);
      instructionIndex += 1;
      state = OP_CODE;
      break;
    }
    }
  }
  
  fclose(input);

  struct bootloaderOutput step1 = runBootloader(instructions);
  printf("%d\n", step1.accumulator);

  struct bootloaderOutput step2 = { 0, false };
  for (int i = 0; instructions[i].opCode > 0 && !step2.ended; i += 1) {
    for (int j = 0; instructions[j].opCode > 0; j += 1) {
      instructions[j].runCount = 0;
    }
    struct instruction* inst = &instructions[i];
    switch (inst->opCode) {
    case JMP: {
      inst->opCode = NOP;
      step2 = runBootloader(instructions);
      inst->opCode = JMP;
      break;
    }
    case NOP: {
      if (inst->value != 0) {
	inst->opCode = JMP;
	step2 = runBootloader(instructions);
	inst->opCode = NOP;
      }
      break;
    }
    }
  }
  printf("%d\n", step2.accumulator);

  
  return 0;
}
