#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PREAMBLE 25
#define CACHE_SIZE PREAMBLE * (PREAMBLE - 1) / 2 + PREAMBLE

extern int readPositiveNumber(FILE* input, char delim);

void readPreamble(FILE* input, int* cache) {
  for (int i = CACHE_SIZE - PREAMBLE; i < CACHE_SIZE; i += 1) {
    cache[i] = readPositiveNumber(input, '\n');
  }

  for (int i = CACHE_SIZE - PREAMBLE, insert = 0; i < CACHE_SIZE; i += 1) {
    for (int j = i + 1; j < CACHE_SIZE; j += 1, insert += 1) {
      cache[insert] = cache[i] + cache[j];
    }
  }
}

bool numberInCache(int next, int* cache) {
  for (int i = 0; i < CACHE_SIZE; i += 1) {
    if (next == cache[i]) {
      return true;
    }
  }
  return false;
}

/* Moves precomputed values in the cache so they don't need to be
 * recalculated. Then, puts new values in place. This makes it an
 * O(m) move.
 *
 * | s12 | s13 | s14 | s23 | s24 | s34 | a1 | a2 | a3 | a4 |      next: a5
 *                                          /    /    /                 |
 * | s12 | s13 | s14 | s23 | s24 | s34 | a2 | a3 | a4 | a5 | <----------+
 *       ______________/     /     /
 *      /     ______________/     /
 *     /     /           ________/
 *    /     /           /
 * | s23 | s24 |     | s34 |     |     | a2 | a3 | a4 | a5 |
 *
 * | s23 | s24 | s25 | s34 | s35 | s45 | a2 | a3 | a4 | a5 |
 *
 */
void shiftCache(int next, int* cache) {
  for (int i = CACHE_SIZE - PREAMBLE; i < CACHE_SIZE - 1; i += 1) {
    cache[i] = cache[i + 1];
  }
  cache[CACHE_SIZE - 1] = next;

  for (int i = 1, t = 0, tn = PREAMBLE - 1; PREAMBLE - i> 0; i += 1, t = tn, tn += PREAMBLE - i) {
    for (int k = 0; k < PREAMBLE - i - 1; k += 1) {
      cache[t + k] = cache[tn + k];
    }
    cache[tn - 1] = cache[CACHE_SIZE - PREAMBLE + i - 1] + cache[CACHE_SIZE - 1];
  }
}

int main() {
  const char* FILE_NAME = "09-input.txt";

  FILE* input;
  if ((input = fopen(FILE_NAME, "r")) == NULL) {
    printf("Error opening %s\n", FILE_NAME);
    return 1;
  }

  int cache[CACHE_SIZE] = { 0 };
  readPreamble(input, cache);
  int next;
  int index = PREAMBLE;

  // Safe because all input are positive integers
  while((next = readPositiveNumber(input, '\n')) > 0 && numberInCache(next, cache)) {
    shiftCache(next, cache);
    index += 1;
  }
  printf("%d\n", next);


  
  fseek(input, 0, SEEK_SET);
  int i;
  int left;
  int right;
  int sum;
  int* range = calloc(index, sizeof(int));
  memset(range, 0, index * sizeof(int));
  range[0] = readPositiveNumber(input, '\n');
  range[1] = readPositiveNumber(input, '\n');
  for (sum = range[0], i = 2, left = 0, right = 1; sum != next &&  right < index; i += 1) {
    if (sum + range[right] <= next) {
      sum += range[right];
      right += 1;
    } else {
      sum -= range[left];
      left += 1;
    }
    if (i < index) {
      range[i] = readPositiveNumber(input, '\n');
    }
  }

  int lo = range[left];
  int hi = range[left];
  for (int i = left; i < right; i += 1) {
    if (range[i] < lo) {
      lo = range[i];
    }
    if (range[i] > hi) {
      hi = range[i];
    }
  }
  printf("%d\n", lo + hi);


  free(range);
  fclose(input);
  return 0;
}
