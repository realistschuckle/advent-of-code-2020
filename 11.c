#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ROWS 95
#define COLS 99

bool readBoards(const char* fileName, int (*boards)[ROWS][COLS]) {
  FILE* input;
  if ((input = fopen(fileName, "r")) == NULL) {
    printf("Error opening %s\n", fileName);
    return false;
  }

  int row = 0, col = 0;
  for (char c = fgetc(input); c != EOF; c = fgetc(input), col += 1) {
    if (c == '\n') {
      row += 1;
      col = -1;
      continue;
    }
    if (c == '.') {
      boards[0][row][col] = -1;
      boards[1][row][col] = -1;
    } else if (c == 'L') {
      boards[0][row][col] = 0;
      boards[1][row][col] = -1;
    }
  }

  fclose(input);
  return true;
}

bool alone1(int (*board)[COLS], int row, int col) {
  if (board[row][col] != 0) {
    return false;
  }
  for (int r = row - 1; r <= row + 1; r += 1) {
    for (int c = col - 1; c <= col + 1; c += 1) {
      if (r < 0 || c < 0 || r >= ROWS || c >= COLS) {
	continue;
      }
      if (board[r][c] == 1) {
	return false;
      }
    }
  }
  return true;
}

bool crowded1(int (*board)[COLS], int row, int col) {
  if (board[row][col] != 1) {
    return false;
  }
  int sum = 0;
  for (int r = row - 1; r <= row + 1; r += 1) {
    for (int c = col - 1; c <= col + 1; c += 1) {
      if (r < 0 || c < 0 || r >= ROWS || c >= COLS) {
	continue;
      }
      if (board[r][c] == 1) {
	sum += 1;
      }
    }
  }
  return sum > 4;
}

enum Directions
  {
   LEFT,
   TOP_LEFT,
   TOP,
   TOP_RIGHT,
   RIGHT,
   BOTTOM_RIGHT,
   BOTTOM,
   BOTTOM_LEFT
  };

void look(int (*board)[COLS], int row, int col, int* sight) {
  int delta;
  for (delta = 1; col - delta >= 0 || row - delta >= 0 || col + delta < COLS || row + delta < ROWS; delta += 1) {
    int left = col - delta;
    int top = row - delta;
    int right = col + delta;
    int bottom = row + delta;
    if (sight[LEFT] == -1 && left >= 0) {
      sight[LEFT] = board[row][left];
    }
    if (sight[TOP_LEFT] == -1 && left >= 0 && top >= 0) {
      sight[TOP_LEFT] = board[top][left];
    }
    if (sight[TOP] == -1 && top >= 0) {
      sight[TOP] = board[top][col];
    }
    if (sight[TOP_RIGHT] == -1 && top >= 0 && right < COLS) {
      sight[TOP_RIGHT] = board[top][right];
    }
    if (sight[RIGHT] == -1 && right < COLS) {
      sight[RIGHT] = board[row][right];
    }
    if (sight[BOTTOM_RIGHT] == -1 && right < COLS && bottom < ROWS) {
      sight[BOTTOM_RIGHT] = board[bottom][right];
    }
    if (sight[BOTTOM] == -1 && bottom < ROWS) {
      sight[BOTTOM] = board[bottom][col];
    }
    if (sight[BOTTOM_LEFT] == -1 && bottom < ROWS && left >= 0) {
      sight[BOTTOM_LEFT] = board[bottom][left];
    }
  }  
}

bool alone2(int (*board)[COLS], int row, int col) {
  if (board[row][col] != 0) {
    return false;
  }
  int sight[8];
  for (int i = 0; i < 8; i += 1) {
    sight[i] = -1;
  }
  look(board, row, col, sight);
  for (int i = 0; i < 8; i += 1) {
    if (sight[i] == 1) {
      return false;
    }
  }
  return true;
}

bool crowded2(int (*board)[COLS], int row, int col) {
  if (board[row][col] != 1) {
    return false;
  }
  int sight[8];
  for (int i = 0; i < 8; i += 1) {
    sight[i] = -1;
  }
  look(board, row, col, sight);
  int sum = 0;
  for (int i = 0; i < 8; i += 1) {
    if (sight[i] == 1) {
      sum += 1;
    }
  }
  return sum >= 5;
}

bool same(int (*boards)[ROWS][COLS]) {
  for (int row = 0; row < ROWS; row += 1) {
    for (int col = 0; col < COLS; col += 1) {
      if (boards[0][row][col] != boards[1][row][col]) {
	return false;
      }
    }
  }
  return true;
}

bool step(int (*boards)[ROWS][COLS], int from, int to, int phase) {
  for (int row = 0, col = 0; row < ROWS; col = (col < COLS - 1) ? col + 1 : 0, row = (col == 0) ? row + 1 : row) {
    if (phase == 1 && alone1(boards[from], row, col)) {
      boards[to][row][col] = 1;
    } else if (phase == 2 && alone2(boards[from], row, col)) {
      boards[to][row][col] = 1;
    } else if (phase == 1 && crowded1(boards[from], row, col)) {
      boards[to][row][col] = 0;
    } else if (phase == 2 && crowded2(boards[from], row, col)) {
      boards[to][row][col] = 0;
    } else {
      boards[to][row][col] = boards[from][row][col];
    }
  }
}

int countSeats(int (*board)[COLS]) {
  int sum = 0;
  for (int row = 0; row < ROWS; row += 1) {
    for (int col = 0; col < COLS; col += 1) {
      if (board[row][col] == 1) {
	sum += 1;
      }
    }
  }
  return sum;
}

char translateDigit(int digit) {
  switch (digit) {
  case -1: {
    return '.';
  }
  case 0: {
    return 'L';
  }
  case 1: {
    return '#';
  }
  }
}

void printBoards(int (*boards)[ROWS][COLS]) {
  char rowAsSymbols[COLS * 2 + 2];
  memset(rowAsSymbols, ' ', COLS * 2 + 2);
  for (int row = 0; row < ROWS; row += 1) {
    for (int col = 0; col < COLS; col += 1) {
      rowAsSymbols[col] = translateDigit(boards[0][row][col]);
      rowAsSymbols[col + COLS + 2] = translateDigit(boards[1][row][col]);
    }
    printf("%s\n", rowAsSymbols);
  }
  printf("\n");
}

int main() {
  const char* FILE_NAME = "11-input.txt";

  int boards[2][ROWS][COLS] = { 0 };

  if (!readBoards(FILE_NAME, boards)) {
    return 1;
  }
  int from = 0;
  int to = 1;
  while (!same(boards)) {
    step(boards, from, to, 1);
    from = 1 - from;
    to = 1 - to;
  }
  printf("%d\n", countSeats(boards[from]));


  if (!readBoards(FILE_NAME, boards)) {
    return 1;
  }
  from = 0;
  to = 1;
  while (!same(boards)) {
    step(boards, from, to, 2);
    from = 1 - from;
    to = 1 - to;
  }
  printf("%d\n", countSeats(boards[from]));

  return 0;
}
