#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_COMMANDS 1024

extern int readPositiveNumber(FILE* file, char delim);

enum Direction
  {
   EAST = 1,
   SOUTH = 2,
   WEST = -1,
   NORTH = -2
  };

struct ship {
  enum Direction dir;
  int x;
  int y;
  int wx;
  int wy;
};

enum Instruction
  {
   NOOP = '*',
   GO_NORTH = 'N',
   GO_SOUTH = 'S',
   GO_EAST = 'E',
   GO_WEST = 'W',
   TURN_LEFT = 'L',
   TURN_RIGHT = 'R',
   FORWARD = 'F'
  };

struct command {
  enum Instruction inst;
  int distance;
};

bool readInput(const char* fileName, struct command* cmds) {
  FILE* input;
  if ((input = fopen(fileName, "r")) == NULL) {
    printf("Error opening %s\n", fileName);
    return false;
  }

  int index = 0;
  char c;
  while ((c = fgetc(input)) != EOF) {
    cmds[index].inst = c;
    cmds[index].distance = readPositiveNumber(input, '\n');
    index += 1;
  }

  fclose(input);
  return true;
}

int main() {
  const char* FILE_NAME = "12-input.txt";

  struct command cmds[MAX_COMMANDS];
  for (int i = 0; i < MAX_COMMANDS; i += 1) {
    cmds[i].inst = NOOP;
    cmds[i].distance = 0;
  }
  if (!readInput(FILE_NAME, cmds)) {
    return 1;
  }

  struct ship s = { EAST, 0, 0, 10, 1 };
  for (int i = 0; cmds[i].inst != NOOP; i += 1) {
    switch (cmds[i].inst) {
    case GO_NORTH: {
      s.y -= cmds[i].distance;
      break;
    }
    case GO_SOUTH: {
      s.y += cmds[i].distance;
      break;
    }
    case GO_EAST: {
      s.x += cmds[i].distance;
      break;
    }
    case GO_WEST: {
      s.x -= cmds[i].distance;
      break;
    }
    case TURN_LEFT: {
      int degrees = cmds[i].distance;
      for (; degrees > 0; degrees -= 90) {
	if (s.dir == NORTH) {
	  s.dir = WEST;
	} else if (s.dir == SOUTH) {
	  s.dir = EAST;
	} else if (s.dir == EAST) {
	  s.dir = NORTH;
	} else if (s.dir == WEST) {
	  s.dir = SOUTH;
	}
      }
      break;
    }
    case TURN_RIGHT: {
      int degrees = cmds[i].distance;
      for (; degrees > 0; degrees -= 90) {
	if (s.dir == NORTH) {
	  s.dir = EAST;
	} else if (s.dir == SOUTH) {
	  s.dir = WEST;
	} else if (s.dir == EAST) {
	  s.dir = SOUTH;
	} else if (s.dir == WEST) {
	  s.dir = NORTH;
	}
      }
      break;
    }
    case FORWARD: {
      if (s.dir == NORTH) {
	s.y -= cmds[i].distance;
      } else if (s.dir == SOUTH) {
	s.y += cmds[i].distance;
      } else if (s.dir == EAST) {
	s.x += cmds[i].distance;
      } else if (s.dir == WEST) {
	s.x -= cmds[i].distance;
      }
    }
    }
  }
  printf("%d\n", s.x + s.y);

  s.x = 0;
  s.y = 0;
  s.wx = 10;
  s.wy = -1;
  for (int i = 0; cmds[i].inst != NOOP; i += 1) {
    switch (cmds[i].inst) {
    case GO_NORTH: {
      s.wy -= cmds[i].distance;
      break;
    }
    case GO_SOUTH: {
      s.wy += cmds[i].distance;
      break;
    }
    case GO_EAST: {
      s.wx += cmds[i].distance;
      break;
    }
    case GO_WEST: {
      s.wx -= cmds[i].distance;
      break;
    }
    case TURN_LEFT: {
      int degrees = cmds[i].distance;
      for (; degrees > 0; degrees -= 90) {
	int tmp = s.wx;
	s.wx = s.wy;
	s.wy = -tmp;
      }
      break;
    }
    case TURN_RIGHT: {
      int degrees = cmds[i].distance;
      for (; degrees > 0; degrees -= 90) {
	int tmp = s.wx;
	s.wx = -s.wy;
	s.wy = tmp;
      }
      break;
    }
    case FORWARD: {
      s.x += s.wx * cmds[i].distance;
      s.y += s.wy * cmds[i].distance;
    }
    }
  }
  printf("%d\n", abs(s.x) + abs(s.y));
  
  return 0;
}
