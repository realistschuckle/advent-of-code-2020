#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

extern int readPositiveNumber(FILE* input, char delim);
extern void quicksortl(long* a, long lo, long hi);

#define MAX_BUSES 128
#define SENTINEL 1000000000

bool readInput(const char* fileName, int* ts, long* ids) {
  FILE* input;
  if ((input = fopen(fileName, "r")) == NULL) {
    printf("Error opening %s\n", fileName);
    return false;
  }

  *ts = readPositiveNumber(input, '\n');

  for (int i = 0; i < MAX_BUSES; i += 1) {
    int id = readPositiveNumber(input, ',');
    ids[i] = id == 0 ? SENTINEL : id;
  }

  fclose(input);
  return true;
}

long modMultInverse(long a, long b) {
  if (b == 1) {
    return 1;
  }

  int base = a % b;
  for (int i = 1; i < b; i += 1) {
    if ((base * i) % b == 1) {
      return i;
    }
  }
  return 1;
}

long chineseRemainder(long* busIds, long* offsets, int len) {
  long product = 1;
  long sum = 0;
  for (int i = 0; i < len; i += 1) {
    product *= busIds[i];
  }
  for (int i = 0; i < len; i += 1) {
    long N = product / busIds[i];
    long inverse = modMultInverse(N, busIds[i]);
    sum += offsets[i] * inverse * N;
  }
  return sum % product;
}

int main() {
  const char* FILE_NAME = "13-input.txt";

  int timestamp = 0;
  long busIds[MAX_BUSES];
  memset(busIds, 0, MAX_BUSES * sizeof(long));
  if (!readInput(FILE_NAME, &timestamp, busIds)) {
    return 1;
  }
  
  long offsets[558];
  memset(offsets, -1, 558 * sizeof(long));

  int high;
  for (high = 0; high < MAX_BUSES && busIds[high] != -1; high += 1) {
    if (busIds[high] != SENTINEL) {
      offsets[busIds[high]] = high;
    }
  }
  quicksortl(busIds, 0, high - 1);
  for (high = 0; high < MAX_BUSES && busIds[high] < SENTINEL; high += 1) {
    offsets[high] = busIds[high] - offsets[busIds[high]];
    while (offsets[high] < 0) {
      offsets[high] += busIds[high];
    }
  }

  for (int i = 0; i < high; i += 1) {
    printf("%4d ", busIds[i]);
  }
  printf("\n");
  for (int i = 0; i < high; i += 1) {
    printf("%4d ", offsets[i]);
  }
  printf("\n");

  int busId = -1;
  int eta;
  for (eta = timestamp; busId < 0; eta += 1) {
    for (int j = 0; j < high; j += 1) {
      if (eta % busIds[j] == 0) {
	busId = busIds[j];
      }
    }
  }
  eta -= 1;
  printf("%d\n", (eta - timestamp) * busId);

  printf("%ld\n", chineseRemainder(busIds, offsets, high));

  return 0;
}


/** COMMENTARY
Here are schedules for two buses. The one marked with A has an id of 7
and an offset of 4. The second bus is marked with B and has an id of 5
and an offset of 0.

   0      7      14     21     28     35     42     49     56     63
4: A------A------A------A------A------A------A------A------A------A
0: B----B----B----B----B----B----B----B----B----B----B----B----B
   0    5    10   15   20   25   30   35   40   45   50   55   60

A solution for where these two properly "align" is at T = 10; at that
point, the B bus arrives at 10 and the A bus arrives 4 units later, the
value of its offset. Here are some statements about those values.

    10 = 2 * 5 + 0  =>  10 ≡ (5 - 0) (mod 5)  =>  10 ≡ 5 (mod 5)
    10 = 2 * 7 - 4  =>  10 ≡ (7 - 4) (mod 7)  =>  10 ≡ 3 (mod 7)

Now, introdude another number and offset.

   0      7      14     21     28     35     42     49     56     63     70     77     84     91
4: A------A------A------A------A------A------A------A------A------A------A------A------A------A
0: B----B----B----B----B----B----B----B----B----B----B----B----B----B----B----B----B----B----B
   0    5    10   15   20   25   30   35   40   45   50   55   60   65   70   75   80   85   90

1: C--C--C--C--C--C--C--C--C--C--C--C--C--C--C--C--C--C--C--C--C--C--C--C--C--C--C--C--C--C--C
   0  3  6  9  12 15 18 21 24 27 30 33 36 39 42 45 48 51 54 57 50 63 66 69 72 75 78 81 84 87 90

Following the pattern before, see the following congruency statements.

    80 = 16 * 5 + 0  =>  80 ≡ (5 - 0) (mod 5)  =>  80 ≡ 5  (mod 5)
    80 = 12 * 7 - 4  =>  80 ≡ (7 - 4) (mod 7)  =>  80 ≡ 3  (mod 7) 
    80 = 27 * 3 - 1  =>  80 ≡ (3 - 1) (mod 3)  =>  80 ≡ 2  (mod 3)

This generalizes to the following statement.

    Find the value T that satisfies the congruency relation

        T ≡ a (mod n), a = n - o

    where n is the bus id and o is the offset.

An application of the Chinese Remainder Theorem helps to solve this.
**/
