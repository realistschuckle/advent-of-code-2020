GCC=gcc -ggdb -std=gnu99
TARGETS=01 02 03 04 05 06 07 08 09 10 11 12 13

default: $(TARGETS)

13: 13.c readNumber.c quicksort.c
	$(GCC) -o $@ $^

12: 12.c readNumber.c
	$(GCC) -o $@ $^

11: 11.c
	$(GCC) -o $@ $^

10: 10.c readNumber.c quicksort.c
	$(GCC) -o $@ $^

09: 09.c readNumber.c
	$(GCC) -o $@ $^

08: 08.c
	$(GCC) -o $@ $^

07: 07.c quicksort.c
	$(GCC) -o $@ $^

06: 06.c
	$(GCC) -o $@ $^

05: 05.c quicksort.c
	$(GCC) -o $@ $^

04: 04.c
	$(GCC) -o $@ $^

03: 03.c
	$(GCC) -o $@ $^

02: 02.c
	$(GCC) -o $@ $^

01: 01.c quicksort.c
	$(GCC) -o $@ $^

clean:
	rm -f $(TARGETS)
