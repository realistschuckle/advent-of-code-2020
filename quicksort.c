void swap(int* a, int i, int j) {
  int tmp = a[i];
  a[i] = a[j];
  a[j] = tmp;
}

int partition(int* a, int lo, int hi) {
  int pivot = a[hi];
  int i = lo;
  for (int j = lo; j < hi; j += 1) {
    if (a[j] < pivot) {
      swap(a, i, j);
      i += 1;
    }
  }
  swap(a, i, hi);
  return i;  
}

void quicksort(int* a, int lo, int hi) {
  if (lo < hi) {
    int p = partition(a, lo, hi);
    quicksort(a, lo, p - 1);
    quicksort(a, p + 1, hi);
  }
}

void swapl(long* a, long i, long j) {
  long tmp = a[i];
  a[i] = a[j];
  a[j] = tmp;
}

long partitionl(long* a, long lo, long hi) {
  long pivot = a[hi];
  long i = lo;
  for (long j = lo; j < hi; j += 1) {
    if (a[j] < pivot) {
      swapl(a, i, j);
      i += 1;
    }
  }
  swapl(a, i, hi);
  return i;  
}

void quicksortl(long* a, long lo, long hi) {
  if (lo < hi) {
    long p = partitionl(a, lo, hi);
    quicksortl(a, lo, p - 1);
    quicksortl(a, p + 1, hi);
  }
}

